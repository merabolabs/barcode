/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,
  TouchableOpacity,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { RNCamera } from 'react-native-camera';
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showCamera: true,
    };
  }


  _onBarCodeRead = (e) => {
    // this.setState({showCamera: false});
    Alert.alert(
      "Barcode Found!",
      "Type: " + e.type + "\nData: " + e.data
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.state.showCamera ?
          
          <RNCamera
            ref="cam"
            style={styles.container}
            onBarCodeRead={this._onBarCodeRead}>
          </RNCamera>

          : <View />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});

export default App;
